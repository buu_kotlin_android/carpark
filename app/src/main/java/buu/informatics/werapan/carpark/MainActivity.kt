package buu.informatics.werapan.carpark

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import buu.informatics.werapan.carpark.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val car0 = Car(0)
    private val car1 = Car(1)
    private val car2 = Car(2)
    private val cars = arrayOf(car0, car1, car2)
    private var currentIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.apply {
            this.cars = this@MainActivity.cars
            roomButton1.setOnClickListener { view ->
                clickRoom(view)
            }
            roomButton2.setOnClickListener { view ->
                clickRoom(view)
            }
            roomButton3.setOnClickListener { view ->
                clickRoom(view)
            }
            saveButton.setOnClickListener { view ->
                saveRoom(view)
            }
        }

    }

    private fun saveRoom(view: View) {
        val car = cars?.get(currentIndex)
        car.empty = false
        binding.invalidateAll()
    }

    private fun getCarIdFormView(view:View):Int {
        when(view.id) {
            R.id.roomButton1 -> return 0
            R.id.roomButton2 -> return 1
            R.id.roomButton3 -> return 2
        }
        return -1
    }

    private fun clickRoom(view: View) {
        binding.apply {
            currentIndex = getCarIdFormView(view)
            this.car = cars?.get(currentIndex)
            invalidateAll()
        }
    }
}
