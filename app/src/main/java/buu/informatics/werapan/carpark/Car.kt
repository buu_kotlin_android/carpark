package buu.informatics.werapan.carpark

data class Car(val id:Int, var name:String=""
                , var carId:String="", var brand:String=""
                , var empty:Boolean=true)